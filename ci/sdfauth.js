const { exec } = require("child_process");

//const realmEnvVar = `realm_${process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME}`;
const realm = process.env.realm_master;
//const tokenEnvVar = `token_${process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME}`;
const token = process.env.token_master;
//const secretEnvVar = `secret_${process.env.CI_MERGE_REQUEST_TARGET_BRANCH_NAME}`;
const secret = process.env.secret_master;

const real_gapsol = process.env.real_gapsol;
const token_gapsol = process.env.token_gapsol;
const secret_gapsol = process.env.secret_gapsol;
const branch = process.env.CI_COMMIT_BRANCH;
if(branch == "master")
{
var authCmd = `suitecloud account:savetoken --account ${real_gapsol} --authid "cisdf" --tokenid ${token_gapsol} --tokensecret ${secret_gapsol}`;
}
else{
  var authCmd = `suitecloud account:savetoken --account ${realm} --authid "cisdf" --tokenid ${token} --tokensecret ${secret}`;
}
exec(authCmd, realm, (error, stdout, stderr) => {
  if (error) {
    console.log(`error: ${error.message}`);
    return;
  }
  if (stderr) {
    console.log(`stderr: ${stderr}`);
    return;
  }
  console.log(`stdout: ${stdout}`);
});